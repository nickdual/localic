class UsersController < ApplicationController
  def index

  end

  def update
    @user = User.find(params[:user][:_id][:$oid])
    if @user.eql?(current_user)
      if !params[:password].blank? and params[:password].to_s.length >= 6
        @user.password = params[:password]
      end
      @user.email = params[:user][:email]
      @user.full_name = params[:user][:full_name]
      @user.username =  params[:user][:username]
      @user.suburb_id =  params[:user][:suburb_id]
        if @user.save
          sign_in(@user, :bypass => true)
          render :json => {:user => @user.as_json ,:status => '200'}
        else
          render :json => {:message => @user.errors.full_messages[0].to_s ,:status => '422'}
        end
    end

  end

  def upload_avatar
    @current_avatar = current_user.avatar
    @avatar = Avatar.new(params.require(:avatar).permit(:image))
    @avatar.user_id = current_user.id if current_user
    if @avatar.save
       current_user.image_url = @avatar.image.small
       current_user.save
       @current_avatar.destroy() if @current_avatar
    end
    render :json => @avatar
  end

  private

  def user_params
    params.required(:user).permit(:email, :full_name, :username, :password, :password_confirmation,:_id, :first_name, :last_name, :suburb, :image_url)
  end

end
