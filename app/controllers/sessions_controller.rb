class SessionsController < Devise::SessionsController

  def destroy
    super

  end

  def create

    @user =  User.where('$or' => [ {:email => params[:user][:login]},{:username => params[:user][:login]} ]).first
    if !@user.present?
      puts "Incorrect Email"
      render :json => {:success => false, :flag => 'not_email', :template => t('sign_in.email_incorrect'), :help => t('sign_in.help_sign_in')}
    else
      resource = warden.authenticate!(:scope => resource_name ,:recall => "sessions#failure")
      puts resource
      set_flash_message(:notice, :signed_in) if is_navigational_format?
      sign_in(resource_name, resource)
      if signed_in?
        render :json =>  {:success => true, :user => resource}
      else
        render :json => {:success => false, :flag => 'password_error', :template => t('sign_in.sign_in_fail'), :help => t('sign_in.help_sign_in')}
      end
    end
  end


  #private
  #def user_params
  #  params.require(:user).permit(:email, :username,:password, :password_confirmation,:remember_me, :login)
  #end

end



