class OmniauthCallbacksController < Devise::OmniauthCallbacksController


  def facebook
    auth()
  end

  private
  def auth
    omni = request.env["omniauth.auth"]
    @token = omni['credentials'].token
    @token_secret = omni['credentials'].secret
    @avatar = omni['info'].image
    @last_name = omni['info'].last_name
    @first_name = omni['info'].first_name
    @user =  User.where(:email => omni['info'].email).first
    if @user
      authentication = @user.authentications.where(:provider => 'facebook',:uid  => '1243664817').first
      if authentication
        flash[:notice] = "Logged in Successfully"
        sign_in_and_redirect @user, :event => :authentication
      else
        authentication = @user.authentications.build(:provider => omni['provider'], :uid => omni['uid'], :token => @toteken, :token_secret => @token_secret)
        if authentication.save
          if @user.image_url.blank?
            @user.update_attribute(:image_url, @avatar)
          end
          flash[:notice] = "Authentication successful."
          sign_in_and_redirect @user
        end
      end
    else
      @user = User.new({image_url: @avatar,last_name: @last_name,first_name: @first_name})
      @user.email = omni['info'].email unless omni['info'].email.blank?
      @user.apply_omniauth(omni)
      @user.skip_confirmation!
      if @user.new_record?
        if @user.save(:validate => false)
          flash[:notice] = "Logged in."
          sign_in User.find(@user.id)
          redirect_to "/users#wellcome"
        else
          session[:omniauth] = omni.except('extra')
          redirect_to new_user_registration_path
        end
      else
        authentication = @user.authentications.build(:provider => omni['provider'], :uid => omni['uid'], :token => @token, :token_secret => @token_secret)
        if authentication.save
          sign_in_and_redirect User.find(@user.id)
        end
      end
    end
  end

end