class SuburbsController < ApplicationController
  def index

  end

  def get_name
    if params[:term] != ''
      @query = /^#{params[:term]}/i
      @suburbs = Suburb.where("name" => @query).limit(5).to_a
      @suburbs.collect! {|item| {:id =>item._id,:name => item.name}}
      render :json => @suburbs
    end
  end
end
