class RegistrationsController < Devise::RegistrationsController
  def create
    @user = User.new(user_params)
    if  @user.save
      flash[:notice] = t("users.confirmationsent")
      render :json => {:user => @user.as_json ,:status => '200'}
    else
      render :json => {:message => @user.errors.full_messages[0].to_s ,:status => '422'}
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :username,:password, :password_confirmation,:full_name, :suburb)
  end
end

