class Localic.Views.Header extends Backbone.View
  el: '#header_page'
  template: JST['headers/header']
  template_success: JST['user_sessions/sign_in_success']
  template_option: JST['user_sessions/menu_option']


  render:() ->
    $(@el).html(@template())
    @show_login_form()
    @render_success()
    @menu_setting()
    @


  render_success:() ->
    if @model
      $(@el).find('.login-label').addClass('hide')
      $(@el).find('.logged-in').html(@template_success({model: @model.toJSON()}))
  show_login_form:(e) ->
    $('.login-label').qtip(
      show: 'click'
      hide: 'unfocus'
      position:
        my: 'top right'
        at: 'bottom right'
        viewport: $(window)
      content:
        text: '<div id="sign_in_form"></div> '
      style:
        classes: 'qtip qtip-default qtip-pos-rt qtip-focus qtip-custom qtip-popover qtip-login'
        tip:
          width: 8
          height: 8
      events:
        render: () ->
          session = new Localic.Views.UserSessionsIndex({el: "##{$(this).find('.qtip-content').attr('id')}"})
          session .render()
      )
  menu_setting:(e) ->
    obj = @
    $(@el).find('.menu-setting').qtip(
      show: 'click'
      hide: 'unfocus'
      position:
        my: 'top right'
        at: 'bottom right'
        viewport: $(window)
      content:
        text: '<div class="menu-status" >  </div>',
      style:
        classes: 'qtip qtip-default qtip-pos-rt qtip-focus qtip-custom qtip-popover qtip-menu'
      tip:
        width: 8
        height: 8
      events:
        render: () ->
          $(this).find('.qtip-content').html(obj.template_option())

    )