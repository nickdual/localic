class Localic.Views.UserSessionsIndex extends Backbone.View
  template: JST['user_sessions/sign_in']
  events:
    'click .login-button': 'login'
    'click .label-forgot-pass' : 'reset_pass_form'
    'click #email': 'close_qtip_email'
    'click #password': 'close_qtip_password'
    'click #user_remember_me': 'init_checkbox'

  initialize: () ->
    @model = new Localic.Models.UserSession()

  render:() ->
    $(@el).html(@template())
    $(@el).find('.great-message').addClass('hide')


  login: (e) ->
    el = $(@el)
    e.preventDefault()
    email = el.find('#email').val()
    password = el.find('#password').val()
    remember_me = el.find('#user_remember_me').val()
    el.find('button.btn-login').button('loading')
    @model.save({user: {login: email, password: password, remember_me: remember_me}},
      success: (userSession, response) ->
        if response.success == true
          $('body').find('.qtip-login').remove()
          Localic.currentUser = new Localic.Models.User(response.user)
          model =  Localic.currentUser
          $('.users-login').find('.login-lable').addClass('hide')
          header = new Localic.Views.Header({model: model})
          header.render()
        else if response.success == false && response.flag == 'not_email'
          el.find('button.btn-login').button('reset')
          el.find('#email').qtip(
            position:
              my: 'left center'
              at: 'right center'
              viewport: $(window)
            content:
              text: '<p class="text"></p>'
            style:
              classes: 'qtip-email'
              tip:
                width: 8
                height: 8
            events:
              render: (e) ->
                $(e.currentTarget).find('.qtip-content').find('p.text').html(response.template)
          ).qtip('show')
        else
          el.find('button.btn-login').button('reset')
          el.find('#password').qtip(
            position:
              my: 'left center'
              at: 'right center'
              viewport: $(window)
            content:
              text: '<p></p>'
            style:
              classes: 'qtip-password'
              tip:
                width: 8
                height: 8
            events:
              render: (e) ->
                $(e.currentTarget).find('.qtip-content').html(response.template)
          ).qtip('show')

      error:(userSession, response) ->
        el.find('button.btn-login').button('reset')
        el.find('#password').qtip(
          position:
            my: 'left center'
            at: 'right center'
            viewport: $(window)
          content:
            text: '<p>Password is incorrect</p>'
          style:
            classes: 'qtip-password'
            tip:
              width: 8
              height: 8
        ).qtip('show')
    )

  reset_pass_form: (e) ->
    reset_pass = new Localic.Views.UserPasswordsIndex()
    reset_pass.render(  )

  close_qtip_email:(e) ->
    $('body').find('.qtip-email').remove()
  close_qtip_password:(e) ->
    $('body').find('.qtip-password').remove()

  init_checkbox: (e) ->
    if $(e.currentTarget).is(':checked')
      $(e.currentTarget).attr('value', 1)
    else
      $(e.currentTarget).attr('value', 0)
