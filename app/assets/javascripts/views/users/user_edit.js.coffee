class Localic.Views.UserEdit extends Backbone.View
  el: "#user_edit"

  template: JST['users/edit']
  events:
    "click .bt-create": "save"
    "click .option" :'option_choice'

  initialize: () ->
    @model = Localic.currentUser
  render: () ->
    if Localic.currentUser
      $("#sign_up").hide()
      $("#user_wellcome").hide()
      @$el.show()
      @$el.html(@template({model: @model}))
      @initValidation()
      @initUploads()
      @init_get_suburb_name()
    else
      window.location = "/"

  initValidation: ->
    $.validator.addMethod "password_min_length", (value)->
      if value.length == 0
        true
      else
        if value.length < 6
          false
        else
          true
    ,"Please enter at least 6 characters."

    $.validator.addMethod "password_max_length", (value)->
      if value.length == 0
        true
      else
        if value.length > 128
          false
        else
          true
    ,"Please enter no more than 128 characters."
    @$("#edit_user").validate
      errorClass: "errormessage"
      rules:
        "user[full_name]":
          required: true
        "user[email]":
          required: true
          email: true
        "user[username]":
          required: true
        "user[password]":
          password_min_length: true
          password_max_length: true

      success: $.noop

      errorPlacement: (error, element) ->
        elem = $(element)
        if !error.is(':empty')
          elem.filter(':not(.valid)').qtip(
            overwrite: false
            content: error
            show:
              event: false,
              ready: true
            hide: false
            position:
              my: 'left center'
              at: 'right center'
              viewport: $(window)
            style:
              classes: 'qtip-password'
              tip:
                width: 8
                height: 8
          ).qtip('show')
            .qtip('option', 'content.text', error)
        else
          elem.qtip('hide')

  save: (e) ->
    target = $(e.currentTarget)
    self = @
    @model.url = "/users/edit_user.json"
    if @$("#edit_user").validate().form()
      target.val("Saving")
      @model.set("email",@$("#user_email").val())
      @model.set("full_name",@$("#user_full_name").val())
      @model.set("suburb_id",@$("#user_suburb").attr('data-id'))
      @model.set("username",@$("#username").val())
      @model.set("password",@$("#user_password").val())
      console.log(@model)
      @model.save( @model.attributes,{
        success: (userSession, response) ->
          target.val("Save")
          if response.status == "200"
            console.log(response.user)
            Localic.currentUser = new Localic.Models.User(response.user)
          else
            error = '<label for="user_email" class="errormessage" style="display: block;">' + response.message + '</label>'
            $("#user_email").qtip(
              overwrite: false
              content: error
              show:
                event: false,
                ready: true
              hide: false
              position:
                my: 'left center'
                at: 'right center'
                viewport: $(window)
              style:
                classes: 'qtip-password'
                tip:
                  width: 8
                  height: 8
            ).qtip('show')
              .qtip('option', 'content.text', error)

      },
        error: (userSession, response) ->
          target.val("Save")
          console.log(response)
      )
    else
    e.preventDefault()

  initUploads: (evt) =>
    self = $(this)
    $("#fileupload").fileupload
      autoUpload: true
      url: '/upload/avatar'
      start: (e) ->
        $("#change_photo").val("Changing")
      added: (e, data) ->
        console.log(data)
        $("#change_photo").val("Changing")
      done: (e, data) ->
        console.log(data.result.thumb)
        $("#user_avatar").attr('src',data.result.thumb)
        Localic.currentUser.set('image_url', data.result.small)
        $("#change_photo").val("Change photo")
        $('#header_avatar').attr('src',data.result.small)


  init_get_suburb_name: () ->
    $('#user_suburb').qtip(
      hide: 'unfocus'
      position:
        my: 'top center'
        at: 'bottom center'
        viewport: $(window)
        container: @$el
      content:
        text: '<ul class="suburbs" style="min-width: 116px"></ul>',
      style:
        classes: 'qtip-custom  qtip-suburb hide'
        tip:
          width: 8
          height: 8
    )
    $('#user_suburb').db_autocomplete(callback: get_suburb)

  get_suburb = (evt) ->
    value = $(this).val()
    if value != ''
      $.ajax(
        type: 'GET',
        url: '/suburbs/get_name',
        async: true,
        jsonpCallback: 'jsonCallback',
        dataType: 'json',
        data: { term: value},
        beforeSend: () ->
        success: (response) ->
          $('body').find('.qtip-suburb').removeClass('hide')
          $('body').find('.qtip-suburb').find('.suburbs').html('')
          if response.length > 0
            _.each(response, (value) ->
              html = '<li class="option opt-hover" value="' + value.id.$oid + '"> <p>' + value.name + '</p></li> '
              evt.parents('#user_edit').find('.qtip-suburb').find('.suburbs').append(html)
            )
          else
            html = '<li class="option"> No sult found </li> '
            $('body').find('.qtip-suburb').find('.suburbs').append(html)
          $('#user_suburb').qtip('show')
      )
  option_choice: (e) ->
    $(e.currentTarget).parents('.qtip-suburb').addClass('hide')
    text = $(e.currentTarget).find('p').text()
    $(e.currentTarget).parents('#user_edit').find('#user_suburb').val(text)
    $(e.currentTarget).parents('#user_edit').find('#user_suburb').attr('data-id',$(e.currentTarget).attr('value'))

