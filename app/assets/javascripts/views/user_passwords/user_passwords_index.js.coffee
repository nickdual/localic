class Localic.Views.UserPasswordsIndex extends Backbone.View
  el: '.forgot-password'
  template: JST['user_passwords/index']
  events:
    'click .reset-button': 'reset_password'
    'click .close-form': 'close_reset_password'
  initialize: () ->
    @model = new Localic.Models.UserPassword()

  render:() ->
    $(@el).html(@template())

  close_reset_password:(e) ->
    $(e.currentTarget).parents('.reset-password-form').remove()

  reset_password: (e) ->
    el = $(@el)
    e.preventDefault()
    email = el.find('#user_email_reset').val()
    el.find('button.btn-reset').button('loading')
    @model.save({user: {email: email}},
      success: (userSession, response) ->
        if response.success == true
          el.parents('.login-page').find('.great-message').removeClass('hide')
          el.html('')
        else if response.success == false && response.flag == 'sent_false'
          alert response.errors
        else
          el.find('button.btn-reset').button('reset')
          el.find('#user_email_reset').qtip(
            position:
              my: 'left center'
              at: 'right center'
              viewport: $(window)
            content:
              text: '<p></p>'
            style:
              classes: 'qtip qtip-default qtip-password'
              tip:
                width: 8
                height: 8
            events:
              render: (e) ->
                $(e.currentTarget).find('.qtip-content').html(response.errors)
          ).qtip('show')
    )