class Localic.Views.UserRegistrationsSuccess extends Backbone.View
  el: "#sign_up_success"

  template: JST['user_registrations/sign_up_success']
  render: ->
    @$el.show()
    @$el.html(@template())


