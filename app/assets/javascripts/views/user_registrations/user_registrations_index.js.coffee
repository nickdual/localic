class Localic.Views.UserRegistrationsIndex extends Backbone.View
  el: "#sign_up"
  events:
    "click .bt-create": "save"
    "click .option" :'option_choice'


  template: JST['user_registrations/index']
  template_suburb_li: JST['shares/suburb_li']

  initialize: () ->
    @model = new Localic.Models.UserRegistration()

  render: ->
    if Localic.currentUser
      window.location = "/"
    else
      $('body').find('.qtip-login').remove()
      @$el.show()
      @$el.html(@template({model: @model}))
      @initValidation()
      @init_get_suburb_name()
    @

  initValidation: ->
    @$("#new_user").validate
      errorClass: "errormessage"
      rules:
        "user[full_name]":
          required: true
        "user[email]":
          required: true
        "user[username]":
          required: true
        "user[password]":
          required: true
          maxlength: 128
          minlength: 6
      success: $.noop

      errorPlacement: (error, element) ->
        elem = $(element)
        if !error.is(':empty')
          elem.filter(':not(.valid)').qtip(
            overwrite: false
            content: error
            show:
              event: false,
              ready: true
            hide: false
            position:
              my: 'left center'
              at: 'right center'
              viewport: $(window)
            style:
              classes: 'qtip-password'
              tip:
                width: 8
                height: 8
          ).qtip('show')
            .qtip('option', 'content.text', error)
        else
          elem.qtip('hide')


  save: (e) ->
    target = $(e.currentTarget)
    self = @
    if @$("#new_user").validate().form()
      target.val("Creating")
      @model.set("email", @$("#user_email").val())
      @model.set("full_name", @$("#user_full_name").val())
      @model.set("suburb_id", @$("#user_suburb").attr('data-id'))
      @model.set("username", @$("#username").val())
      @model.set("password", @$("#user_password").val())
      @model.set("password_confirmation", @$("#user_password").val())
      @model.save( @model.attributes,{
        success: (userSession, response) ->
          if response.status == "200"
            $("#sign_up_success").show()
            console.log(response.user)
            Localic.currentUser = new Localic.Models.User(response.user)
            self.remove()
            view = new  Localic.Views.UserRegistrationsSuccess()
            view.render()
          else
            target.val("Create Account")
            error = '<label for="user_email" class="errormessage" style="display: block;">' + response.message + '</label>'
            $("#user_email").qtip(
              overwrite: false
              content: error
              show:
                event: false,
                ready: true
              hide: false
              position:
                my: 'left center'
                at: 'right center'
                viewport: $(window)
              style:
                classes: 'qtip-password'
                tip:
                  width: 8
                  height: 8
            ).qtip('show')
              .qtip('option', 'content.text', error)
      },
        error: (userSession, response) ->
          console.log(response)
      )
    e.preventDefault()

  init_get_suburb_name: () ->
    $('#user_suburb').qtip(
      hide: 'unfocus'
      position:
        my: 'top center'
        at: 'bottom center'
        viewport: $(window)
        container: @$el
      content:
        text: '<ul class="suburbs"></ul>',
      style:
        classes: 'qtip-custom qtip-suburb hide'
        tip:
          width: 8
          height: 8
    )
    $('#user_suburb').db_autocomplete(callback: get_suburb)

  get_suburb = (evt) ->
    value = $(this).val()
    if value != ''
      $.ajax(
        type: 'GET',
        url: '/suburbs/get_name',
        async: true,
        jsonpCallback: 'jsonCallback',
        dataType: 'json',
        data: { term: value},
        beforeSend: () ->
        success: (response) ->
          $('body').find('.qtip-suburb').removeClass('hide')
          $('body').find('.qtip-suburb').find('.suburbs').html('')
          if response.length > 0
            _.each(response, (value) ->
              html = '<li class="option opt-hover" value="' + value.id.$oid + '"> <p>' + value.name + '</p></li> '
              evt.parents('#sign_up').find('.qtip-suburb').find('.suburbs').append(html)
            )
          else
            html = '<li class="option"> No sult found </li> '
            $('body').find('.qtip-suburb').find('.suburbs').append(html)
          $('#user_suburb').qtip('show')
      )

  option_choice: (e) ->
    $(e.currentTarget).parents('.qtip-suburb').addClass('hide')
    text = $(e.currentTarget).find('p').text()
    $(e.currentTarget).parents('#sign_up').find('#user_suburb').val(text)
    $(e.currentTarget).parents('#sign_up').find('#user_suburb').attr('data-id',$(e.currentTarget).attr('value'))


