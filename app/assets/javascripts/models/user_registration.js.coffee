class Localic.Models.UserRegistration extends Backbone.Model
  url: '/users.json'
  paramRoot: 'users'

  defaults:
    "username": ""
    "email": ""
    "password": ""
    "password_confirmation": ""

  validate: (attrs, options) ->
    if attrs.username == ""
      return "Please choose an image at least"
    if attrs.email == ""
      return "Please input email"
    if attrs.password= ""
      return "Please input password"
  toJSON: (options) ->
    return {user : _.extend(this.attributes, options)}

  save: (attrs, options) ->
    options || (options = {})
    options.contentType = 'application/json'
    options.data = JSON.stringify(_.extend(this.toJSON(attrs), options.data))
    Backbone.Model.prototype.save.call(this, attrs, options)
