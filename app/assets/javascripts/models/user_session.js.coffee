class Localic.Models.UserSession extends Backbone.Model
  url: '/users/sign_in.json',
  paramRoot: 'users',

  defaults:
    "email": "",
    "password": ""
