class Localic.Models.User extends Backbone.Model
  idAttribute: "_id"
  paramRoot: 'users'
  urlRoot: "/users"
  defaults:
    first_name: null
    last_name: null
    username: null
    email: null