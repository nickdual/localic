window.Localic =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  initialize: ->
    new Localic.Routers.UserRegistrations()
$(document).ready ->
  Localic.initialize()
  Backbone.history.start({pushState: false});
