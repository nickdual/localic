class Localic.Routers.UserRegistrations extends Backbone.Router
  routes: 
    "sign_up": "sign_up"
    "sign_up_success": "sign_up_success"
    "edit_user": "edit_user"
    "wellcome": "wellcome"

  sign_up_success: () ->
    view = new  Localic.Views.UserRegistrationsSuccess()
    view.render()

  sign_up: () ->
    view = new  Localic.Views.UserRegistrationsIndex()
    view.render()

  edit_user: () ->
    view = new  Localic.Views.UserEdit()
    view.render()

  wellcome: () ->
    view = new  Localic.Views.UserWellCome()
    view.render()
