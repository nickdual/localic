(($) ->
  $.fn.db_autocomplete = (options) ->
    defaults = {
      callback: () ->
    }
    opts = $.extend(defaults, options);
    $(document).on('keyup', this.selector, () ->
      opts.callback.call($(this), $(this))
    )

    $(document).on('click', this.selector, (evt) ->
      return false;
    )

    $(document).on('focusin', this.selector, (evt) ->
      auto = $(this).parent().find('.autocomplete')
      if (auto.css('display') == 'none')
        $('.autocomplete').hide()
        auto.show()
      else
        auto.hide()

      evt.preventDefault()
      evt.stopPropagation()
      return false;
    )

    $(document).on('click', '.autocomplete li', (evt) ->
      $(this).parent().hide()
      text = $(this).find('.email').text()
      $(this).parent().parent().parent().children('input').val(text)
      evt.preventDefault()
    )

    $(document).on('click', 'body', () ->
      auto = $('.autocomplete')
      if (auto.length > 0)
        auto.hide()
    )
#    $(document).on('focusin', 'input[class!=autocomplete_event]', () ->
#      auto = $('.autocomplete')
#      if (auto.length > 0)
#        auto.hide()
#    )
)(jQuery);