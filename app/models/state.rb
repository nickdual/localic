class State
  include Mongoid::Document
  field :name , :type => String
  field :shortname, :type => String
  field :country => Integer
  has_many :suburbs
end
