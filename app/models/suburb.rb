class Suburb
  include Mongoid::Document
  field :name , :type => String
  field :postcode, :type => String
  field :lat , :type => Float
  field :lng , :type => Float
  field :state_id , :type => String
  belongs_to :state
end
