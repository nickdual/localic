class Avatar
  include Mongoid::Document
  field :url, :type => String
  field :image, :type => String
  field :description, :type => String
  field :image_type, :type => String
  mount_uploader :image, AvatarUploader
  belongs_to :user

  def small
    self.image.small.to_s
  end

  def thumb
    self.image.thumb.to_s
  end

  def serializable_hash(options={})
    super(options.reverse_merge(:methods => [:thumb,:small], :except => :image))
  end
end
