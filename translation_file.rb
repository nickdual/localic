table "states" do
	column "id", :key, :as => :integer
	column "name", :string
	column "shortname", :string
	column "country", :integer
end

table "suburbs", :belongs_to => :states,  :as => :object do
	column "id", :key, :as => :integer
	column "name", :string
	column "postcode", :string
	column "state", :integer, :rename_to => 'state_id'   , :references => :states
	column "lat", :float
	column "lng", :float

end
